<?php

return [
    'sitename' => 'Fiery Me',
    'domain' => 'sikkepingin',
    // 'storename' => Lang::get('common.sell'),
    'protocol' => 'http',
    'uploadmaxdim' => 800,
    'resetpasswordkey' => 'resetPassword',
    'thumbdim' => 100,
    'itemperpage' => 10,
    'prodimgdim' => array(
        (object) array('dir' => 'sm', 'width' => 292, 'height' => 232), 
        (object) array('dir' => 'md', 'width' => 583, 'height' => 463), 
        (object) array('dir' => 'lg', 'width' => 875, 'height' => 695)
    ),
    'cryptlabel' => (object) array(
        'firstname' => '1B7hsWS9lkfVHunGT9',
        'lastname' => 'EQfraCYYz0Gh',
        'username' => 'eyJpdiI6Im4ya1B',
        'email' => 'NHRIb3U5QV',
    ),
    'honeypottime' => 5,
    'facebook' => (object) array(
        // 'base_url' => Request::getHost().'/fbauth/auth',
        'providers' => array(
            'Facebook' => array(
                'enabled' => TRUE,
                'keys' => array('id' => '1452218451744573', 'secret' => '5b543771860ff849bd81d9e5d6b0af82'),
                'scope' => 'public_profile,email'
            )
        )
    ),
    'userkey' => 'useronline',
    'urls' => array(
        // 'orders' => URL::asset('/orders')
    ),
    'currency' => array(
        (object) array(
            'name' => 'Indonesia Rupiah',
            'id' => 'id_ID',
            'code' => 'IDR'
        ),
        (object) array(
            'name' => 'United States Dolar',
            'id' => 'en_US',
            'code' => 'USD'
        )
    ),
    'carrier' => array(
        (object) array(
            'name' => 'JNE',
            'type' => array('OKE','REG', 'YES')
        ),
        (object) array(
            'name' => 'POS INDONESIA',
            'type' => array('reguler', 'kilat')
        )
    ),
    'bankaccount' => array(
        (object) array(
            'BankName' => 'Bank Central Asia (BCA)',
            'AccountHolder' => 'Irsyad Fadlillah',
            'AccountNumber' => '4329-2323-12323-343',
            'BankDetail' => 'BCA Malang Martadinata',
        )
    ),
    'transcation_type' => array(
        (object) array(
            'name' => 'Debt',
            'color' => 'label-success',
            'type' => 'plus'
        ),
        (object) array(
            'name' => 'Adm',
            'color' => 'label-default',
            'type' => 'min'
        ),
        (object) array(
            'name' => 'Withdraw',
            'color' => 'label-success',
            'type' => 'min'
        )
    ),
    'messagestype' => array(
        'inbox' => 0,
        'draft' => 1,
        'trash' => 2,
        'sent' => 3
    ),
    'maxtag' => 5,
    'group' => array(
        'citizen',
        'seller',
        'adminstaff'
    ),
    'courier' => array(
        (object) array(
            'id' => 'JNE',
            'name' => 'JNE'
        )
    ),
    'imagesqrdim' => 200,
    'imagewalldim' => (object) array('width' => 640, 'height' => 480),
    'imagegalllg' => 1024,
    'imagegallsm' => 150,
    'pricing' => array(
        (object) array(
            'name' => 'Starter Pack',
            'antrian_limit' => 20,
            'staff_limit' => 1,
            'price_id' => array()
        ),
        (object) array(
            'name' => 'Medium Pack',
            'antrian_limit' => 'Unlimited',
            'staff_limit' => 1,
            'price_id' => (object) array(
                'yearly' => 360000,
                'monthly' => 40000
            )
        ),
        (object) array(
            'name' => 'Saver Pack',
            'antrian_limit' => 'Unlimited',
            'staff_limit' => 1,
            'price_id' => (object) array(
                'yearly' => 400000,
                'monthly' => 50000
            ),
            'desc' => (object) array('id' => 'Hanya di charge jika jumlah booking melebihi limit')
        )
//        (object) array(
//            'name' => 'Premium Pack',
//            'antrian_limit' => 'Unlimited',
//            'staff_limit' => 'Unlimited',
//            'price_id' => (object) array(
//                'yearly' => 2000000,
//                'monthly' => 200000
//            )
//        )
    ),
    'notifications' => array(),
    // 'logourl' => URL::asset('images/logo.jpg'),
    // 'wallurl' => URL::asset('lib/umega/build/images/backgrounds/06.jpg'),
    'addimageurl' => 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI2NCIgaGVpZ2h0PSI2NCI+PHJlY3Qgd2lkdGg9IjY0IiBoZWlnaHQ9IjY0IiBmaWxsPSIjZWVlIi8+PHRleHQgdGV4dC1hbmNob3I9Im1pZGRsZSIgeD0iMzIiIHk9IjMyIiBzdHlsZT0iZmlsbDojYWFhO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1zaXplOjEycHg7Zm9udC1mYW1pbHk6QXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWY7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+NjR4NjQ8L3RleHQ+PC9zdmc+',
    'address' =>  '<address><strong>Twitter, Inc.</strong><br>795 Folsom Ave, Suite 600<br>San Francisco, CA 94107<br><abbr title="Phone">P:</abbr> (123) 456-7890<br><a href="mailto:#">first.last@example.com</a></address>',
    // 'defaultavatar' => URL::asset('images/avatar/a.jpg'),
    'status' => array(
        '0' => array(
            'type' => 'alert-warning',
            'status' => 'Waiting Payment Confirmation',
            'help' => 'Click confirm payment to confirm your payment or click view detail to view your detail payment'
         ),
        '1' => array(
            'type' => 'alert-info',
            'status' => 'Order Procesing',
            'help' => 'Seller is processing this order'
         ),
        '2' =>  array(
            'type' => 'alert-info',
            'status' => 'Payment Confirmed',
            'help' => 'Your payment is received and confirm, seller will continue process your order'
         )
    ),
    'subscription_type' => array('monthly', 'yearly'),
    'useradminid' => array(1, 3),
    'contact' => array(
        'address' => 'Pesona Mutiara Residence Blok L2 Pepe Sedati',
        'city' => 'Sidoarjo',
        'country' => 'Indonesia',
        'phone' => '+62 8233-120-8887',
        'email' => 'info@linelaze.com'
    )
];

//var entityMap = {
//        "&": "&amp;",
//        '"': "&quot;",
//        "'": "&#39;",
//        " ": "&#nbsp;"
//    };