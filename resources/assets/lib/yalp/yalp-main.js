/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Title:   Yalp, App Landing Page - Main Javascript file
 * Author:  http://themeforest.net/user/5studios | www.5studios.net
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

// Avoid `console` errors in browsers that lack a console.
(function() {
    'use strict';

    var method;
    var noop = function() {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

// Run when DOM is ready
$(window).load(function() {
    'use strict';

    /**
     * LOADER
     * When the DOM es finally ready, remove the pre-loader
     **/
    $("#front-loader")
        .delay(3000)
        .fadeOut("slow");
});

$(document).ready(function() {
    'use strict';

    /**
     * WOW
     * Initialize nice animations when scrolling
     **/
    new WOW({
        mobile: false,
        boxClass: "animate",
        callback: function(el) {
            function detectAnimationEvent(el) {
                var animations = {
                    "animation": "animationend",
                    "OAnimation": "oAnimationEnd",
                    "MozAnimation": "animationend",
                    "WebkitAnimation": "webkitAnimationEnd",
                    "MsAnimationEnd ": "msAnimationEnd"
                };

                for (var t in animations) {
                    if (el.style[t] !== undefined) {
                        return animations[t];
                    }
                }
            }

            var animationend = detectAnimationEvent(el);

            $(el).one(animationend, function(event) {
                var $this = $(this);
                var animation = $this.data("animation");

                if (animation) {
                    $this.removeAttr("style").removeClass().addClass(animation);
                }
            });
        }
    }).init();

    /**
     * STICKY MENU
     * Make the navbar fixed to top when scrolling
     **/
    var navigation = $("#main-navigation"),
        navigationHeight = navigation.height();

    function navbarSticky() {
        if ($(window).scrollTop() > navigationHeight) {
            navigation.addClass("navbar-sticky");
        } else {
            navigation.removeClass("navbar-sticky");
        }
    }

    $(window).scroll(function() {
        navbarSticky();
    });

    navbarSticky();

    /**
     * TYPED HEADER
     **/
    $(".typed-features").typed({
        strings: [
            "^2000 Simple to use",
            "Powerful",
            "Well documented",
            "Creative",
            "Light code",
            "Easy customizable"
        ]
    });

    /**
     * SCROLLING NAVIGATION
     * Enable smooth transition animation when scrolling
     **/
    var scrollAnimationTime = 1200,
        scrollAnimation = 'easeInOutExpo';
    $('nav#main-navigation .navbar-nav a, a.scrollto').on('click', function(event) {
        var target = this.hash;
        if (!$(target).offset()) {
            return true;
        }
        event.preventDefault();


        $('html, body').stop().animate({
            scrollTop: $(target).offset().top
        }, scrollAnimationTime, scrollAnimation, function() {
            window.location.hash = target;
        });
    });

    /* COLLAPSE MOBILE TOP NAVIGATION
     * Collapse navbar menu when clicked an option
     */
    $(document).on('click', '.navbar-collapse.in', function(e) {
        if ($(e.target).is('a')) {
            $(this).collapse('hide');
        }
    });

    /**
     * SCREENSHOTS CAROUSEL
     * For the screenshots we'll do in two steps:
     * 1. Carousel
     * 2. Fullscreen lightbox
     **/
    $("#screenshots-slides").owlCarousel({
        autoPlay: 5000, //Set AutoPlay to 5 seconds
        loop: true,

        items: 6,
        itemsDesktop: [1200, 5],
        itemsDesktopSmall: [900, 3],
        itemsTablet: [600, 2],
        itemsMobile: [400, 1]
    });

    $('#screenshots-slides a').nivoLightbox({
        theme: 'yalp',
        nav: {
            prev: '',
            next: '',
            close: ''
        },
        afterShowLightbox: function() {
            $('.nivo-lightbox-overlay').addClass('overlay overlay-1 alpha-75');
        },

        onPrev: function() {
            $('.nivo-lightbox-content .nivo-lightbox-image')
                .addClass('animated slideInLeft');
        },
        onNext: function() {
            $('.nivo-lightbox-content .nivo-lightbox-image')
                .addClass('animated slideInRight');
        }
    });

    /**
     * TESTIMONIALS CAROUSEL
     * Testimonials only need one step and one item at once
     **/
    $("#testimonials-slides").owlCarousel({
        autoPlay: 5000, //Set AutoPlay to 5 seconds
        singleItem: true
    });
});