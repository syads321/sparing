

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">


    <title>Starter Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    

    <!-- Custom styles for this template -->
   

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

    -->
       </head>
       <style>
        .mid {
            display:block;
            float:none;
            margin-left :auto;
            margin-right:auto;
        }
       </style>

  <body  ng-app="app" ng-controller="cartCtrl">

    <div class="container">
      <div class="col-md-7">
        <table class="table">
            <th>
                <td>Product Name</td>
                <td>Quantity</td>
                <td>Price Per Product</td>
                <td>Total</td>
            </th>
            <tr ng-repeat="product in products track by $index">
                <td><% $index %></td>
                <td><% product.name %></td>
                <td><% product.quantity %></td>
                <td><% product.priceperitem %></td>
                <td><% product.total %></td>
            </tr>
            <tr>
                <td colspan="4">Total</td>
                <td><% totalProduct(products) %></td>
            </tr>
        </table>
      </div>
      <div class="col-md-7">
        <form >
            <div class="form-group">
                <label for="name">Product Name</label>
                <input type="text" class="form-control" id="name" ng-model="product.name" placeholder="Product Name">
            </div>
            <div class="form-group">
                <label for="quantity">Quantity</label>
                <input type="text" class="form-control" id="quantity" ng-model="product.quantity" placeholder="Quantity">
            </div>
             <div class="form-group">
                <label for="pricepertiem">Price Per Item</label>
                <input type="text" class="form-control" id="pricepertiem" ng-model="product.priceperitem" placeholder="Price per Item">
            </div>
            <a class="btn btn-default" ng-click="addProduct(product)">Submit</a>

            </form>
      </div>

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="{{URL::asset('/lib/angular/angular.min.js')}}"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script>
        var token = "{{csrf_token()}}";
        var root = "{{URL::asset('/')}}";root 
        var app = angular.module('app', [], function($interpolateProvider) {
            $interpolateProvider.startSymbol('<%');
            $interpolateProvider.endSymbol('%>');
        });
        app.controller('cartCtrl', function($scope, $http) {
                $scope.product = {};
                $scope.products = [];
                $scope.product.name = 'myproduct';
                $scope.product.quantity = 5;
                $scope.product.priceperitem = 1000;
                $scope.addProduct = function(product) {
                    product.id = $scope.products.length + 1;
                    product.total = product.quantity * product.priceperitem;
                   
                    $scope.products.push(product);
                    $scope.product._token = token;
                    $http.post(root + '/api/addproduct', $scope.products, {}).then(function(resp) {
                        console.log(resp)
                    }, function() {

                    });
                }
                $scope.totalProduct = function(product) {
                    var total = 0;
                    console.log
                    angular.forEach(product, function(val, key) {
                        total  = total + val.total;
                    })
                    return total;
                }
        });
    
    </script>
  </body>
</html>