<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Pagination Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used by the paginator library to build
	| the simple pagination links. You are free to change them to anything
	| you want to customize your views to better match your application.
	|
	*/

	'slogan' => ' <h2>Antrian <span class="hilite">pelanggan anda</span></h2>
                                <p>
                                    <span class="bold">dimana saja, kapan saja</span><br>
                                </p>',
    'allyouneed' => '<b>Semua</b> yang anda butuhkan dari system antrian.',
	'easytouse' => 'Penggunaan Mudah',
	'designedsimplicity' => 'Didesain untuk <span class="bold">simplicity</span>',
	'turnsmartphone' => 'Mengubah <span class="bold">tablet / smartphone</span> menjadi mesin antrian touchscreen',
	'bookanywhere' => 'Ambil / pantau antrian dimana saja',
	'bookweb' => '<small><span class="bold">Ambil dan lihat</span> antrian lewat smartphone / komputer</small>',
	'bookurl' => '<p>Setiap mesin antrian memiliki <span class="bold">www.linelaze.com/nama.bisnis.anda</span> untuk lihat dan ambil nomor antrian</p>',
	'printersupport' => 'Dukungan Printer',
	'printdirect' => '<small><span class="bold">Langsung cetak</span> nomor antrian</small>',
	'printnumber' => '<p>Langsung cetak <span class="bold">nomor antrian</span> seperti layaknya mesin antrian konvensional.</p>',
	'queuenumbesound' => 'Suara Panggilan Antrian',
	'queunnumbecalling' => 'Panggilan <span class="bold">suara</span> nomor antrian',
	'queuenumber' => 'Devices akan \'memanggil\' nomor antrian sesuai dengan urutan',
	'multiplequeuoutlet' => 'Beberapa Loket Antrian',
	'supportmultipleoutlet' => 'Mendukung lebih dari satu <span class="bold">loket antrian </span>',
	'supportmultipleoutletdesc' => 'Bisa digunakan untuk tempat antrian dengan loket / operator lebih dari satu',
	'sms_notification_sdes' => 'Pengiriman<span class="bold">SMS</span> secara otomatis',
	'sms_notification_des' => 'Ketika antrian sudah dekat, Devices secara otomatis akan mengirimkan <span class="bold">SMS</span> ke user yang booking jarak jauh.',
	'yourfeedback' => '<b>Saran Anda</b> membangun linelaze',
	'isinactivedev' => 'Linelaze saat ini sedang aktif dikembangkan untuk memecahkan solusi antrian untuk semua orang.',
	'sayhello' => '<b>Kirimkan pesan anda</b> atau hanya mengatakan \'hello\' ' ,
	'footerslogan' => 'Mesin antrian <b>tanpa menunggu</b>.'
);