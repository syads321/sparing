<?php

return array(
    'name' => 'Nama',
    'phonenumber' => 'Nomor Telepon',
    'slogan' => 'System antrian tanpa tunggu ditempat',
    'username' => 'Nama User',
    'email' => 'Email',
    'forgetpassword' => 'Lupa Password ?',
    'password' => 'Password',
    'login' => 'Masuk',
    'alreadyregister' => 'Sudah Punya Username ?',
    'registercreate' => 'Daftar dan Buat Tempat Antrian',
    'createqueue' => 'Buat Tempat Antrian Anda',
    'invoices' => 'Tagihan',
    'usersetting' => 'Pengaturan User',
    'help' => 'Bantuan',
    'logout' => 'Keluar',
    'hasplaceid' => 'Saya Memilik Kode Tempat',
    'searchplace' => 'Cari Tempat',
    'relogin' => 'Login User Lain',
    'displaymode' => 'Mode Tampilan',
    'bookurladdress' => 'Alamat URL Booking',
    'bookcount' => 'Jumlah Ambil Antrian',
    'daterange' => 'Range Tanggal',
    'from' => 'dari',
    'to' => 'hingga',
    'stats' => 'Statistik',
    'bookbymonth' => 'Jumlah Antrian Bulan ini',
    'bookperdayrate' => 'Rata rata antrian per Hari',
    'staff' => 'Staff',
    'nowqueue' => 'Antrian saat ini',
    'fullqueue' => 'Antrian penuh mohon ditunggu sebentar',
    'queuename' => 'Nama tempat antrian',
    'placename' => 'Nama Tempat',
    'example' => 'Contoh',
    'city' => 'Kota',
    'cityplaceholder' => 'Malang',
    'address' => 'Alamat',
    'myqueuepoint' => 'Tempat Antrian Saya',
    'addressexample' => 'Jalan Merdeka Barat No 196',
    'save' => 'Simpan',
    'presstobook' => 'Tekan tombol dibawah untuk mengambil nomor antrian',
    'bookqueue' => 'Ambil Nomor Antrian',
    'democode' => 'Kode Demo',
    'continuetutorial' => 'Lanjutkan Tutorial',
    'key' => 'Kode',
    'processed' => 'Diproses',
    'call' => 'Panggil',
    'recall' => 'Panggil Ulang',
    'subscription_type' => 'Tipe Langganan',
    'queue_limit' => 'Antrian Limit',
    'subscription_end_date' => 'Tanggal Akhir Berlangganan',
    'staff_limit' => 'Staff Limit',
    'subscription_upgrade' => 'Upgrade Langganan',
    'pickbluetooth' => 'Pilih Printer Bluetooth',
    'sms_notification' => 'Pemberitahuan SMS', 
    'sms_booking' => 'SMS Booking',
    'sms_booking_number' => 'Nomor SMS Booking',
    'sms_booking_desc' => 'Booking antrian lewat SMS',
    'sms_pending' => 'Pending SMS',
    'sms_pending_desc' => 'Jumlah pending antrian saat sms dikirimkan',
    'sound_rate' => 'Kecepatan Suara',
    'upgrade_queue' => 'Upgrade Antripoint',
    'yearly' => 'Per Tahun',
    'monthly' => 'Per Bulan',
    'total' => 'Jumlah',
    'account_holder' => 'Nama Pemegang Rekening',
    'account_number' => 'Nomor Rekening',
    'account_bankname' => 'Nama Bank',
    'account_bankdetil' => 'Detail Bank',
    'target_bank_payment' => 'Rekening Tujuan Pembayaran',
    'confirm_payment' => 'Konfirmasi Pembayaran',
    'thanks_for_upgrade' => 'Terima kasih atas upgrade Anda',
    'will_process_upgrade' => 'Setelah menerima konfirmasi pembayaran Anda, kami akan segera memproses upgrade Anda',
    'choose_printer' => 'Pilih Printer',
    'dashboard_help' => '<p>Pada bagian dashboard terdapat statistik dari antrian anda tiap hari</p>
                <p>Anda juga bisa mengatur kecepatan suara panggilan, sms pemberitahuan atau printer pada bagian setting </p>
                <p>Disarankan untuk mengklaim kepemilikan antrian dengan dengan membuat user anda untuk memastian anda adalah pengelola tempat antrian. Meskipun login menggunakan kode tempat juga bisa dilakukan.</p>',
    'ok_got_it' => 'Baik, Saya mengerti',
    'kiosk_help' => '<p>Ini adalah mode kiosk digunakan untuk</p>
                <ul>
                    <li>Mengambil antrian ditempat</li>
                    <li>Mencetak nomor antrian melalui printer bluetooth</li>
                    <li>Memanggil pengunjung sesuai dengan nomor antrian</li>
                </ul>
                <p>Antrian bisa diambil lewat : </p>
                <ul>
                    <li>Menekan tombol ambil antrian pada mode kiosk</li>
                    <li>SMS ke smartphone ini dengan format : linelaze book</li>
                    <li>Booking online melalui website : <strong>http://www.linelaze.com/{{url}}</strong></li>
                </ul>
                <p>Pada mode kiosk pengambilan nomor antrian bisa dilakukan langsung oleh pengunjung, atau lewat operator </p>
                <p>Untuk mensimulasikan anda sebagai pengunjung, mohon tekan tombol antrian beberapa kali</p>',
    'operator_mode' => 'Mode Operator',
    'operator_help' => '<p>Mode operator digunakan untuk mengelola antrian.</p>
                <p>Default ditampilkan hanya antrian yang belum terproses. Tekan tombol <i class=" fa fa-archive"></i> untuk melihat semua antrian</p>
                <p>Tombol<strong> \'Panggil Antrian\'</strong>, untuk memproses antrian.<br> Tombol <strong>\'Panggil Ulang\'</strong> untuk memanggil ulang antrian.<br> Devices lain yang terinstall dengan id yang sama pada mode Kiosk akan mengeluarkan suara
                    sesuai dengan nomor antrian </p>
                <p>Tiap Antrian memiliki key untuk memverifikasi nomor antrian dengan pemilik nomor antrian apabila tidak memiliki tiket antrian atau mengambil antrian lewat website.</p>',
    'recommend_place' => 'Rekomendasikan Tempat',
    'invoice.waitingpaymentconfirm'=> 'Menunggu Konfirmasi',
    'invoice.paymentconfirmed' => 'Pembayaran Telah Diterima',
    'invoice.ordersucced'=> 'Pembelian Berhasil',
    'hello' => 'Hallo',
    'more' => 'Lebih Lanjut',
    'contactus' => 'Hubungi kami',
    'questionandfeedback' => 'Untuk saran dan pertanyaan silahkan menghubungi kami.',
    'yourmessage' => 'Pesan Anda',
    'yourname' => 'Nama Anda',
    'subject' => 'Subjek',
    'validemail' => 'Alamat email yang benar',
    'sendmessage' => 'Kirim Pesan',
    'typecaptchaabove' => 'Ketikkan nomor captcha diatas',
    'features' => 'Fitur',
    'contact' => 'Kontak'
);