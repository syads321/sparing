<?php

return array(
    'name' => 'Name',
    'phonenumber' => 'Phone Number',
    'slogan' => 'Free no in place waiting for everyone',
    'username' => 'User Name',
    'email' => 'Email',
    'forgetpassword' => 'Forget Password ?',
    'password' => 'Password',
    'login' => 'Login',
    'alreadyregister' => 'Already register ?',
    'registercreate' => 'Create Queue',
    'createqueue' => 'Create your queue place',
    'invoices' => 'Invoices',
    'usersetting' => 'User Setting',
    'help' => 'Help',
    'logout' => 'Log Out',
    'hasplaceid' => 'I have place code',
    'myqueuepoint' => 'My Queue Point',
    'searchplace' => 'Search Place',
    'relogin' => 'Relogin',
    'displaymode' => 'Display Mode',
    'bookurladdress' => 'Url Book Address',
    'bookcount' => 'Booking Count',
    'daterange' => 'Date Range',
    'from' => 'from',
    'to' => 'to',
    'stats' => 'Statistic',
    'bookbymonth' => 'Book by Month',
    'bookperdayrate' => 'Book per Day',
    'staff' => 'Staff',
    'nowqueue' => 'Current Queue',
    'fullqueue' => 'Queue is full, please wait',
    'queuename' => 'Queue Name',
    'placename' => 'Place Name',
    'example' => 'Example',
    'city' => 'City',
    'cityplaceholder' => 'New York',
    'address' => 'Alamat',
    'addressexample' => 'Jalan Merdeka Barat No 196',
    'save' => 'Save',
    'presstobook' => 'Press button to book queue',
    'bookqueue' => 'Book Queue',
    'democode' => 'Demo Code',
    'continuetutorial' => 'Continue Tutorial',
    'key' => 'Code',
    'processed' => 'Processed',
    'call' => 'Call',
    'recall' => 'Recall',
    'subscription_type' => 'Subscription Type',
    'queue_limit' => 'Queue Limit',
    'subscription_end_date' => 'Subscription End Date',
    'staff_limit' => 'Staff Limit',
    'subscription_upgrade' => 'Upgrade Subcription',
    'pickbluetooth' => 'Choose Bluetooth Printer',
    'sms_notification' => 'SMS Notification', 
    'sms_booking' => 'SMS Booking',
    'sms_booking_number' => 'SMS Booking Number',
    'sms_booking_desc' => 'Book by SMS to this devices',
    'sms_pending' => 'Pending SMS',
    'sms_pending_desc' => 'Amount of pending book when notification sms send',
    'sound_rate' => 'Sound Rate',
    'upgrade_queue' => 'Upgrade Queue Subscription',
    'yearly' => 'Yearly',
    'monthly' => 'Monthly',
    'total' => 'Total',
    'account_holder' => 'Account Holder',
    'account_number' => 'Account Number',
    'account_bankname' => 'Bank Name',
    'account_bankdetil' => 'Bank Detail',
    'target_bank_payment' => 'Target Payment',
    'confirm_payment' => 'Confirm Payment',
    'thanks_for_upgrade' => 'Thanks for your upgrade',
    'will_process_upgrade' => 'We will process upgrade shortly after payment confirmed.',
    'choose_printer' => 'Choose Printer',
    'dashboard_help' => '<p>In the dashboard, you can check your daily quues statisctic</p>
                <p>You can also set sound speed rate, sms notification also printer in the setting section.</p>
                <p>Recommended to claim queue authorization by creating user, however login using place code is allowed.</p>',
    'ok_got_it' => 'Okey, I got it',
    'kiosk_help' => '<p>Kiosk Mode used for:</p>
                <ul>
                    <li>Book queue</li>
                    <li>Print queue number using bluetooth printer</li>
                    <li>Emit queue number sound when the queu is about to executed</li>
                </ul>
                <p>Queue can be booked via : </p>
                <ul>
                    <li>Press book queue in kiosk mode</li>
                    <li>Send sms to this device using format : linelaze book</li>
                    <li>Book via website using this url : <strong>http://www.linelaze.com/{{url}}</strong></li>
                </ul>
                <p>You can display kiosk mode directly to visiter so that visitor able to book queue independently or by operator</p>
                <p>To stimulate as your visitor, please press book queu several times</p>',
    'operator_mode' => 'Operator Mode',
    'operator_help' => '<p>Operator mode used to manage queue.</p>
                <p>Default display only unexecuted queueu. To display all queue press <i class=" fa fa-archive"></i> button</p>
                <p>Press<strong> \'Call Queue Antrian\'</strong>, to call queue.<br> Press <strong>\'Recall\'</strong> to recall queue.<br> Another devices installed with same id will also emitt queue number sound</p>
                <p>Every queue number have a key to verify queue number owner when queue book via website.</p>',
   'recommend_place' => 'Recommend Place',
   'invoice.waitingpaymentconfirm'=> 'Waiting payment confirmation',
   'invoice.paymentconfirmed' => 'Payment Confirmed',
   'invoice.ordersucced'=> 'Order Succed',  
   'hello' => 'Hello',
   'more' => 'More',
   'contactus' => 'Contact Us',
   'questionandfeedback' => 'For question and feedback please contact us.',
   'yourmessage' => 'Your Message',
   'yourname' => 'Your Name',
   'subject' => 'Subject',
   'validemail' => 'Valid Email Address',
   'sendmessage' => 'Send Message',
   'typecaptchaabove' => 'Type Captcha Above',
   'features' => 'Features',
   'contact' => 'Contact'
);