<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Pagination Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used by the paginator library to build
	| the simple pagination links. You are free to change them to anything
	| you want to customize your views to better match your application.
	|
	*/

	'slogan' => ' <h2><span class="hilite">No In Place Waiting </span></h2>

                                <p>
                                    <span class="bold">Queue System</span><br>
                                </p>',
    'allyouneed' => '<b>All</b> you need from queue system.',
	'easytouse' => 'Easy To Use',
	'designedsimplicity' => 'Designed for <span class="bold">simplicity</span>',
	'turnsmartphone' => 'Turn <span class="bold">tablet / smartphone</span> touchscreen queue system',
	'bookanywhere' => 'Book / check queue anywhere',
	'bookweb' => '<small><span class="bold">Book and check</span>  queue by smartphone / computer</small>',
	'bookurl' => '<p>Every queue point have an  <span class="bold">www.linelaze.com/your.bussines.name</span> to check and book queue number</p>',
	'printersupport' => 'Printer Support',
	'printdirect' => '<small><span class="bold">Direct</span> queue number printing</small>',
	'printnumber' => '<p>Directly <span class="bold">print queu number</span> just like konvensional queue kiosk.</p>',
	'queuenumbesound' => 'Queue number calling sound',
	'queunnumbecalling' => 'Calling queue number <span class="bold">sound</span>',
	'queuenumber' => 'Devices able to \'call\' current queue',
	'multiplequeuoutlet' => 'Multiple queue outlet',
	'supportmultipleoutlet' => 'Support multiple <span class="bold">queue outlet </span>',
	'supportmultipleoutletdesc' => 'Support queue point with multiple outlet',
	'sms_notification_sdes' => 'Send notification<span class="bold">SMS</span> automatically',
	'sms_notification_des' => 'SMS notification will send to registered number when queue will about to excuted.',
	'yourfeedback' => '<b>Your feedback</b> build linelaze',
	'isinactivedev' => 'Linelaze is in active development to solve queue problem for everyone',
	'sayhello' => '<b>Send your message </b> just say hello \'hello\' ' ,
	'footerslogan' => '<b>No In Place Waiting</b> Queue System.'
);