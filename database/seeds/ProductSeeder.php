<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = 50;
        $data = array();
        for($i = 0;$i < $count; $i++) {
            array_push($data, [
            'name' => str_random(10),
            ]);
        }
        DB::table('products')->insert($data);
        //
    }
}
