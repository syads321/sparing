<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryTodolistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_todolist', function (Blueprint $table) {
           $table->integer('category_id')->unsigned()->nullable();
           $table->foreign('category_id')->references('id');

           $table->integer('todolist_id')->unsigned()->nullable();
           $table->foreign('todolist_id')->references('id');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('category_todolist');
    }
}
