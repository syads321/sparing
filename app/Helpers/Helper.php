<?php

namespace App\Helpers;
use Config;
abstract class Helper
{
    //
    public static function nameToUrl($string, $param = null, $post = false) 
    {
        $scope = (object) array();
        $scope->names = Config::get('app.namespace');
        $scope->classes = explode ('/', $string);
        $type = array('page');
        if ($post == true) {
            $type = array('post');
        }
        array_splice($scope->classes, 0, 0, $type);
        $scope->url = '';
        foreach($scope->classes as $key => $value) {
            $value[0] = strtolower($value[0]);
            $scope->url = $scope->url.'/'.Helper::camelCaseToDash($value);
        }
        $scope->param = $param;
        if (isset($param) === true) {
            $scope->url = $scope->url.'?';
            $scope->lastkey = count($param) - 1;
            // $scope->count = 0;
            foreach($param as $key => $value) {
                $scope->url = $scope->url.$key.'='.$value;
                if ($scope->count !== $scope->lastkey) {
                $scope->url = $scope->url.'&&';
                }
                // $scope->count++;
                //$scope->url = $scope->url.$key.'='.$value.'&&';
            }
        }
        return $scope->url;
    }
    public static function dashesToCamelCase($string, $capitalizeFirstCharacter = false) {

        $str = str_replace(' ', '', ucwords(str_replace('-', ' ', $string)));

        if (!$capitalizeFirstCharacter) {
            $str[0] = strtolower($str[0]);
        }

        return $str;
    
    }
    public static function camelCaseToDash($string) 
    {
        return strtolower(preg_replace('/([A-Z])/', '-$1', $string));
    }

    public static function urlToName($name) {
        $splitted = explode('/', $name);
        $name = array();
       
        $namespaces = array(Config::get('app.namespace'));
        $namespace = Config::get('app.namespace');
        array_splice($splitted, 1, 0, array('aa'));
        foreach($splitted as $Key => $value) {
            $title = ucwords(strtolower($value));
            array_push($name, ucwords(Helper::dashesToCamelCase($title)));
        }
        $backslash = "\\";
        $lastindex = count($name) - 1;
        $method = ucwords(strtolower($name[$lastindex]));
        unset($name[$lastindex]);
        $classname = implode($backslash, $name);
        if ($method == $namespace) {
            $method = $classname;
            $classname = $classname.$backslash.$namespace;
        }
        return (object) array(
            'name' => $classname,
            'method' => 'index',
            'subclass' => $method
        );
        
    }

    public static function consoleDebug($data) {
        return '<script type="text/javascript">var data = '.json_encode($data).'; console.log(data);</script>';
    }
    
    public static function generateGuid() {
         return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

            // 16 bits for "time_mid"
            mt_rand( 0, 0xffff ),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand( 0, 0x0fff ) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand( 0, 0x3fff ) | 0x8000,

            // 48 bits for "node"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }
}
