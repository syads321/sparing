<?php

namespace App\Helpers;
use Response as RespIns;
use Request;

class Response extends Helper
{
    //
    public static function post($url = '', $messages = array(), $data = array()) {
        $json = Request::ajax();
        if ($json == true) {
            return Response::json(array_merge($data, array('messages' => $messages)))
                ;
        }
        else {
            Session::flash('notifications', $messages);
            return Redirect::to($url);    
        }
        return true;
    }

    public static function get($key) {
        $data = RedisIns::get($key);
        return json_decode($data);
    }

}