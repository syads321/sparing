<?php

namespace App\Helpers;
use Redis as redisIns;

class Redis extends Helper
{
    //
    public static function set($key, $val) {
        RedisIns::set($key, json_encode($val));
        return true;
    }

    public static function get($key) {
        $data = RedisIns::get($key);
        return json_decode($data);
    }

    public static function publish($channel, $message) {
        RedisIns::publish($channel, json_encode($message));
    }
}