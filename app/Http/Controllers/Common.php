<?php

namespace App\Http\Controllers;
use App\Helpers\Helper;
use App\Helpers\Redis;
use App\Model\Product;
use DB;
use Request;
use File;
class Common extends Controller
{
    public static $namespace = 'App\Http\Controllers\Apis\\';
    public function debug($id) {
        $this->data = Redis::get($id);
        return Helper::consoleDebug($this->data);
    }

    public function index() {
        
    }

    public function page($slashData) {
        $this->fromurl = Helper::urlToName($slashData);
        //App\Http\Controllers\
        //$name = 'App\Http\Controllers\Apis\\'. $this->fromurl->name.'\\'.$this->fromurl->subclass;
        $name = Common::$namespace. $this->fromurl->name.'\\'.$this->fromurl->subclass;
        $method = $this->fromurl->method;
        if (class_exists($name)) {
            $this->classcall = new $name();
            // if (method_exists($this->classcall, 'validate')) {
            //     return 'false';
            // }
            // else {
            //     return $this->classcall->$method();
            // }
            return $this->classcall->$method();
        }
        else {
            abort(404);
        }
    }

    public function post($slashData) {
        $this->fromurl = Helper::urlToName($slashData);
        $name = Common::$namespace. $this->fromurl->name.'\\'.$this->fromurl->subclass;
        $method = $this->fromurl->method;
   
        if (class_exists($name)) {
            $this->classcall = new $name();
            // if (method_exists($this->classcall, 'validate')) {
            //     return 'false';
            // }
            // else {
            //     return $this->classcall->$method();
            // }
             
            return $this->classcall->$method();
        }
        else {
            abort(404);
        }
    }

    public function addProduct() {
        $this->all = Request::all();
        $this->path = public_path.'/product.json';
        $this->data = json_decode($this->path);
        RedisSet('debug', $this->data);
        return response()->json(array('success' => true));
    }
}