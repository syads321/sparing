<?php
namespace App\Http\Controllers\Apis\Queuepoint\Aa;

use App\Model\Queue;
use App\Model\QueuepointStats;
use App\Model\Queuepoint;
use Request;
use Response;
use App\Helpers\Helper;
use App\Helpers\Redis;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Config;
// use Config;

// $queupoint = new Queupoint();
// $queupoint->Book = function() {
//     return 'hello book';
// };

class Book extends Queuepoint 
{
   function __construct() 
   {
      parent::__construct();
   }

   private static function validate() {
     
   }

   public function index($test = false, $input = array()) 
   {
     if ($test == true) {
        $this->all = $input;
     }
     else {
        $this->all = Request::all();
     }
    
     $this->presentday = $this->all['date'];
     $this->mdkey = Helper::generateGuid();
     $this->exploded = explode('-', $this->mdkey);
     $this->maxnum = Queue::where('antrianpoint_id', '=', $this->all['antrian_id'])
                    ->where('created_at', '<', $this->presentday.'  23:59:59')
                    ->where('created_at', '>', $this->presentday.'  00:00:00')
                    ->max('number');
     if ($this->maxnum === null) {
            $this->yesterdays = Carbon::createFromTimeStamp(strtotime($this->presentday))->subDays(1)->toDateString();
            Queue::where('antrianpoint_id', '=', $this->all['antrian_id'])
                        ->where('created_at', '<=', $this->yesterdays.' 23:59:59')->delete();
         $this->maxnum = 0;
      }
      $this->antripointquery = Queuepoint::whereId($this->all['antrian_id'])->get()->toArray();
      $this->sms_notification = $this->antripointquery[0]['sms_notification'];
      $this->insertdata = array(
//            'user_id' => $this->all['user_id'],
//            'phone_number' => $this->all['phone_number'],
            'antrianpoint_id'=> $this->all['antrian_id'],
            'created_at' => date('Y-m-d H:i:s'),
            'number' => $this->maxnum + 1,
            'antrian_key' => $this->exploded[0],
//            'phone_number' => $this->all['phone_number']
      );
     
      $json = Request::ajax();
     
      if (($json == false) && ($this->sms_notification == 1)) {
            $this->insertdata['phone_number'] = $this->all['phone_number'];
      }
      
      Queue::insert($this->insertdata);
      
      // $this->query = \DB::table('antrian')->orderBy('created_at', 'desc')->limit(1)
      //       ->join('users', 'antrian.user_id', '=', 'users.id')
      //       ->select('users.id', 'users.username', 'users.firstname', 'users.lastname', 'antrian.created_at', 'antrian.number')
      //       ->get()[0];
      $message = (object) array(
            'antrianpoint_id' => $this->all['antrian_id'],
            'carrier' => Queuepoint::board($this->all['antrian_id'], $this->presentday)
            //'carrier' => (object) array()
      );
      $message->carrier->list = Queuepoint::listQueue($this->all['antrian_id'], $this->presentday);
      
      QueuepointStats::addCount($this->all['antrian_id'], $this->presentday);
     
      Redis::publish(Config::get('app.channel'), $message);
      
      return Response()->json(['success' => true]);
   }
}