<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('form.cart');
});

Route::get('debug/{id}', 'Common@debug');
//Route::get('page/{name}', 'Common@page')->where('slashData', '(.*)');
Route::get('page/{slashData}', array('uses' => 'Common@page','as' => '{slashData}'))->where('slashData', '(.*)');
//Route::post('api/{slashData}', array('uses' => 'Common@post','as' => '{slashData}'))->where('slashData', '(.*)');
// Route::post('/api/queuepoint/book', function() {
//         return Response()->json(['success' => true]);
// });

Route::post('api/addproduct', 'Common@addProduct');
